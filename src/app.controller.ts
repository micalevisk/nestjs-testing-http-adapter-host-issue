import { Controller, Get, Inject, HttpServer } from "@nestjs/common";

@Controller()
export class AppController {
  constructor(
    @Inject("foo") private readonly foo: HttpServer,
  ) {}

  @Get()
  isHttpHostDefined() {
    return !!this.foo;
  }
}
