import { Module, Inject, HttpServer } from "@nestjs/common";
import { HttpAdapterHost } from "@nestjs/core";
import { AppController } from "./app.controller";

@Module({
  imports: [],
  controllers: [AppController],
  providers: [
    {
      provide: "foo",
      inject: [HttpAdapterHost],
      useFactory: (httpAdapterHost: HttpAdapterHost) => httpAdapterHost.httpAdapter,
      // useFactory: (httpAdapterHost) => httpAdapterHost,
    },
  ],
})
export class AppModule {
  constructor(@Inject("foo") private readonly foo: HttpServer) {}
  onModuleInit() {
    console.log("on module init:", this.foo);
  }
}
